# Python 파일 처리: 읽기, 쓰기 및 추가Python 모듈: 임포트, 생성 및 사용 <sup>[1](#footnote_1)</sup>

> <font size="3">Python의 내장 함수를 사용하여 파일을 읽고 쓰고 추가하는 방법에 대해 알아본다.</font>

## 목차

1. [소개](./file-handling.md#intro)
1. [Python에서 파일이란?](./file-handling.md#sec_02)
1. [Python에서 파일을 열고 닫는 방법](./file-handling.md#sec_03)
1. [Python으로 파일을 읽는 방법](./file-handling.md#sec_04)
1. [Python에서 파일을 쓰고 추가하는 방법](./file-handling.md#sec_05)
1. [파일 작업의 예외 및 오류를 처리하는 방법](./file-handling.md#sec_06)
1. [Python에서 다양한 파일 형식으로 작업하는 방법](./file-handling.md#sec_07)
1. [요약](./file-handling.md#summary)


<a name="footnote_1">1</a>: [Python Tutorial 17 — Python File Handling: Read, Write, and Append](https://levelup.gitconnected.com/python-tutorial-17-python-file-handling-read-write-and-append-c596eb1c21a9?sk=ec7a54a8be6158a64060ed03e4c104c4) 를 편역한 것이다.
