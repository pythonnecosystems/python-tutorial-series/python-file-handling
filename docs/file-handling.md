# Python 파일 처리: 읽기, 쓰기 및 추가

## <a name="intro"></a> 소개
파일 처리는 프로그래머, 특히 데이터 분석가와 과학자들에게 가장 중요한 기술 중 하나이다. 파일 처리를 통해 텍스트 파일, CSV 파일, JSON 파일 등 다양한 타입의 파일에 저장된 데이터를 읽고 쓰고 조작할 수 있다.

이 포스팅에서는 파일을 열기, 닫기, 파일에서 데이터 읽기, 파일에 데이터 쓰기 및 첨부하기, 파일 작업 중 발생할 수 있는 예외 및 오류 처리 등 파일 처리 작업을 수행하기 위해 Python의 내장 함수를 사용하는 방법에 대해 다룰 것이다. 또한 Python에서 일반 텍스트, 바이너리, CSV, JSON 등 다양한 파일 형식으로 작업하는 방법도 살펴볼 것이다.

이 글을 마치면 다음 작업을 수행할 수 있을 것이다.

- Python에서 파일이란 무엇이며 `open()`과 `close()` 함수를 사용하여 파일을 열고 닫는 방법을 이해한다.
- `read()`,` readline()`, `readlines()` 및 `with` 문과 같은 다양한 메서드를 사용하여 파일에서 데이터를 읽을 수 있다.
- `write()`, `writelines()`, 및 `with` 문과 같은 다양한 방법을 사용하여 파일에 데이터를 쓰고 추가할 수 있다.
- `try`, `except`, `finally`와 `raise` 문을 사용하여 파일 작업 중에 발생할 수 있는 예외 및 오류를 처리할 수 있다.
- Python에서 일반 텍스트, 바이너리, CSV, JSON 등과 같은 다양한 파일 형식으로 작업할 수 있다.

시작하기 전에 컴퓨터에 Python이 설치되어 있는지 확인하세오. 또한 Python 코드를 작성하고 실행하려면 텍스트 편집기 또는 IDE(통합 개발 환경)가 필요하다. Visual Studio Code, PyCharm, Sublime Text 등 원하는 텍스트 편집기 또는 IDE를 사용할 수 있다.

Python에서 파일을 다루는 법을 배울 준비가 되었나요? 시작해 봅시다!

## <a name="sec_02"></a> Python에서 파일이란?
파일은 디스크나 메모리 장치에 저장된 데이터의 집합이다. 파일은 텍스트, 이미지, 오디오, 비디오 등과 같은 모든 타입의 데이터를 포함할 수 있다. 파일은 또한 파일의 속성을 설명하는 이름, 확장자, 크기, 위치 및 기타 속성을 가질 수 있다.

Python에서 파일은 컴퓨터의 물리적인 파일을 나타내는 객체이다. Python의 내장 함수와 메서드를 사용하여 파일에 대해 열기, 닫기, 읽기, 쓰기, 추가하기 등 다양한 작업을 수행할 수 있다.

Python에서 파일을 작업하려면 파일 경로와 파일 모드라는 두 가지 개념을 이해해야 한다.

### 파일 경로 (File Paths)
파일 경로는 컴퓨터에서 파일의 위치를 지정하는 문자열이다. 파일 경로는 절대적이거나 상대적일 수 있다.

- **절대(absolute) 파일 경로**는 컴퓨터의 루트 디렉터리에서 시작하여 모든 하위 디렉터리와 파일 이름을 포함한다. 예를 들어 `"C:\Users\Alice\Documents\example.txt"`는 Windows 컴퓨터의 사용자 `"Alice"`의 `"Documents"` 폴더에 있는 `"example.txt"` 파일의 절대 파일 경로이다.
- **상대(relative) 파일 경로**는 Python 프로그램의 현재 작업 디렉터리에서 시작하여 하위 디렉터리와 파일 이름만 포함한다. 예를 들어, Python 프로그램이 사용자 `"Alice"`의 `"Documents"` 폴더에 있다면 `"example.txt"`는 위와 같은 파일에 대한 상대 파일 경로이다.

Python에서 `os` 모듈을 사용하여 Python 프로그램의 현재 작업 디렉토리를 변경할 수 있다. 예를 들어 `os.getcwd()` 함수를 사용하여 현재 작업 디렉토리를 가져올 수 있고 `os.chdir()` 함수를 사용하여 변경할 수 있다.

```python
# Import the os module
import os

# Get the current working directory
print(os.getcwd())
# Change the current working directory
os.chdir("C:\Users\Alice\Downloads")
# Get the new current working directory
print(os.getcwd())
```

### 파일 모드 (File Mode)
파일 모드는 Python에서 파일을 열 방법을 지정하는 문자열이다. 파일 모드는 파일의 동작과 형식을 나타내는 하나 이상의 문자를 가질 수 있다. 가장 일반적인 파일 모드는 다음과 같다:

- **`"r"`**: 읽기 모드. 읽기 전용으로 파일을 연다. 파일 포인터는 파일의 처음에 위치한다. 이것이 디폴트 모드이다.
- **`"w"`**: 쓰기 모드. 쓰기 전용으로 파일을 연다. 파일이 있다면 덮어쓰거나 파일이 없으면 새 파일을 만든다. 파일 포인터는 파일의 맨 앞에 놓인다.
- **`"a"`**: 추가 모드. 추가 전용으로 파일을 연다. 파일의 끝에 데이터를 기록한다. 파일 포인터는 파일의 끝에 놓인다. 파일이 없으면 새 파일을 만든다.
- **`"r+"`**: 읽기와 쓰기 모드. 읽기와 쓰기 모두를 위한 파일을 연다. 파일 포인터는 파일의 맨 앞에 놓인다.
- **`"w+"`**: 쓰기와 읽기 모드. 쓰기와 읽기 모두를 위한 파일을 연다. 파일이 있으면 덮어쓰거나 파일이 없으면 새 파일을 만든다. 파일 포인터는 파일의 맨 앞에 놓인다.
- **`"a+"`**: 추가 및 읽기 모드. 추가와 읽기 모두를 위한 파일을 연다. 파일의 끝에 데이터를 쓴다. 파일 포인터는 파일의 끝에 놓인다. 파일이 없으면 새 파일을 만든다.
- **`"x"`**: 전용 생성 모드. 생성 모드 전용으로만 파일을 연다. 파일이 이미 있으면 실패한다.
- **`“t”`**: 텍스트 모드. 텍스트 모드로 파일을 연다. 이것이 디폴트 모드이다.
- **`“b”`**: 이진 모드. 이진 모드로 파일을 연다.

파일 모드를 결합하여 다양한 조합을 얻을 수 있다. 예를 들어, `"rb"`는 읽기와 이진 모드로 파일을 열고, `"wt"`는 쓰기와 텍스트 모드로 파일을 연다.

이제 Python에서 파일이란 무엇이고 경로와 모드를 지정하는 방법을 알아 보았다. 이제 Python에서 파일을 열고 닫는 방법을 알아보자.

## <a name="sec_03"></a> Python에서 파일을 열고 닫는 방법
Python에서 파일을 열기 위해서는 `open()` 함수를 사용한다. `open()` 함수는 파일 경로와 파일 모드의 두 인자를 매개 변수로 사용한다. 파일 경로는 컴퓨터에서 파일의 위치를 지정하는 문자열이다. 파일 모드는 파일을 여는 방법을 지정하는 문자열이다. 예를 들어, 읽기 모드는 `"r"`, 쓰기 모드는 `"w"`, 추가 모드는 `"a"` 등을 사용할 수 있다. 파일 모드를 결합하여 읽기와 이진 모드는 `"rb"`, 쓰기와 텍스트 모드는 `"wt"` 등과 같이 다양한 조합을 얻을 수 있다.

`open()` 함수는 파일에 대한 읽기, 쓰기, 첨부하기 등의 다양한 작업을 수행하는 데 사용할 수 있는 파일 객체를 반환한다. 예를 들어, 파일의 내용을 읽는 데 `read()` 메서드, 파일에 데이터를 쓰는 데 `write()` 메서드, 파일을 닫는 데 쓰는 `close()` 메서드 등을 사용할 수 있다.

다음은 Python에서 파일을 열고 그 내용을 읽는 방법의 예이다.

```python
# Open a file in read mode
file = open("example.txt", "r")

# Read the contents of the file
data = file.read()
# Print the data
print(data)
# Close the file
file.close()
```

Python에서 파일을 열었다면 파일을 다 사용한 다음에 항상 파일을 닫아야 한다. 파일을 닫으면 파일과 관련된 리소스가 가용하게 되고, 다른 프로그램이 파일을 수정할 경우 발생할 수 있는 오류나 손상을 방지할 수 있다. `close()` 메서드를 사용하여 파일 객체를 닫을 수 있다. 예를 들어 `file.close()`는 `file`이라는 파일 객체를 닫는다.

그러나 Python에서 파일을 열고 닫을 때 더 좋고 우아한 방법이 있다. Python에서는 `with` 문을 사용할 수 있다. `with` 문은 파일의 개폐를 자동으로 처리해주는 컨텍스트(context) 매니저이다. `with` 문을 사용할 때 `close()` 메서드를 명시적으로 호출할 필요가 없다. `with` 문은 파일 작업 중 예외나 오류가 발생하더라도 파일이 정확히 닫히는 것을 보장한다.

다음은 Python에서 파일을 열고 닫기 위해 `with` 문을 사용하는 예이다.

```python
# Use the with statement to open a file in read mode
with open("example.txt", "r") as file:
    # Read the contents of the file
    data = file.read()
    # Print the data
    print(data)
# The file is automatically closed at the end of the with block
```

보시다시피 `with` 문은 파일 처리 과정을 단순화하고 코드를 더 읽기 쉽고 강력하게 만든다. 따라서 Python에서 파일 작업을 할 때마다 `with` 문을 사용하는 것을 권장한다.

이제 Python에서 파일을 열고 닫는 방법을 알았으니 Python에서 파일을 읽는 방법을 알아보자.

## <a name="sec_04"></a> Python으로 파일을 읽는 방법
Python으로 파일을 읽기 위해서는 파일 객체에 다양한 메서드와 함수를 사용할 수 있다. 가장 일반적인 메서드와 함수는 다음과 같다.

- **`read()`**: 이 메서드는 파일의 전체 내용을 하나의 문자열로 읽는다. 선택적으로 인수로 읽을 바이트 수를 지정할 수 있다. 예를 들어 `file.read()`는 전체 파일을 읽는 반면 `file.read(10)`는 파일의 처음 10바이트를 읽는다.
- **`readline()`**: 이 메서드는 파일에서 한 줄을 문자열로 읽는다. 한 줄은 새 줄 문자(`\n`)로 끝나는 문자의 시퀀스로 정의된다. 예를 들어 `file.readline()`은 파일의 첫 줄을 읽고, 다음 `file.readline()`은 파일의 두 번째 줄을 읽는다.
- **`readlines()`**: 이 메서드는 파일에서 모든 줄을 읽고 문자열 리스트로 반환한다. 리스트의 각 문자열은 파일의 한 줄을 나타낸다. 예를 들어 `file.readlines()`는 파일의 모든 줄 리스트를 반환한다.
- **`for loop`**: 파일의 행를 반복하기 위해 `for loop`을 사용할 수도 있다. 이는 파일을 한 줄씩 읽는 보다 효율적이고 우아한 방법이다. 예를 들어, `for line in file:` 는 파일의 각 행을 루프해서 변수 `line`에 할당한다.

다음은 Python에서 파일을 읽기 위해 이러한 메서드와 함수를 사용하는 예이다.

```python
# Use the with statement to open a file in read mode
with open("example.txt", "r") as file:
    # Read the entire contents of the file as a single string
    data = file.read()
    print(data)
    # Move the file pointer back to the beginning of the file
    file.seek(0)
    # Read one line from the file as a string
    line = file.readline()
    print(line)
    # Move the file pointer back to the beginning of the file
    file.seek(0)
    # Read all the lines from the file and return them as a list of strings
    lines = file.readlines()
    print(lines)
    # Move the file pointer back to the beginning of the file
    file.seek(0)
    # Use a for loop to iterate over the lines of the file
    for line in file:
        print(line)
```

보다시피 Python에서 파일을 읽을 때 필요와 선호도에 따라 다른 메서드와 함수를 사용할 수 있다. 그러나 이러한 메서드와 함수는 파일 포인터의 위치에 영향을 주어 예상치 못한 결과를 초래할 수 있으므로 혼합하지 않도록 주의해야 한다. `seek()` 메서드를 사용하여 파일 포인터를 파일 내의 특정 위치로 이동시킬 수 있다. 예를 들어 `file.seek(0)`은 파일 포인터를 파일의 시작 부분으로 이동시킬 것이다.

이제 Python으로 파일을 읽는 방법을 알았다. Python으로 파일에 쓰고 추가하는 방법을 알아보자.

## <a name="sec_05"></a> Python에서 파일을 쓰고 추가하는 방법
Python에서 파일에 쓰거나 추가하려면 파일 객체의 `write()` 또는 `writeline()` 메서드를 사용하면 된다. `write()` 메서드는 문자열을 인수로 받아 파일에 쓴다. `writeline()` 메서드는 문자열 목록을 인수로 받아 각 문자열을 별도의 행으로 파일에 쓴다.

이러한 방법을 사용하려면 파일을 쓰기 또는 추가 모드로 열어야 한다. `"w"` 또는 `"a"` 파일 모드를 사용하여 쓰기 또는 추가하기 위해 파일을 열 수 있다. 또한 `"w+"` 또는 `"a+"` 파일 모드를 사용하여 쓰기와 읽기 모두를 위해 파일을 열 수 있다. 그러나 이러한 모드를 사용할 때 파일의 기존 데이터에 덮어쓰거나 이를 삭제하지 않도록 주의해야 한다.

다음은 Python에서 `write()` 및 `writeline()` 메서드를 사용하여 파일을 작성하고 추가하는 방법의 예이다.

```python
# Use the with statement to open a file in write mode
with open("example.txt", "w") as file:
    # Write a string to the file
    file.write("This is a new file.\n")
    # Write another string to the file
    file.write("This is the second line.\n")
# Use the with statement to open the same file in append mode
with open("example.txt", "a") as file:
    # Write a list of strings to the file as separate lines
    file.writelines(["This is the third line.\n", "This is the fourth line.\n"])
# Use the with statement to open the same file in read mode
with open("example.txt", "r") as file:
    # Read the contents of the file
    data = file.read()
    # Print the data
    print(data)
```

보다시피 `write()`와 `writeline()` 메서드를 사용하면 Python에서 파일에 데이터를 쓰고 추가할 수 있다. 그러나 이러한 메서드는 문자열이나 목록의 끝에 새 줄 문자(`\n`)를 자동으로 추가하지는 않는다. 파일에서 줄을 분리하려면 새 줄 문자를 직접 추가해야 한다.

이제 Python으로 파일을 작성하고 첨부하는 방법을 알았다. 파일 작업 중에 발생할 수 있는 예외와 오류를 처리하는 방법을 알아보자.

## <a name="sec_06"></a> 파일 작업의 예외 및 오류를 처리하는 방법
Python의 파일 작업은 프로그램의 정상적인 실행을 방해할 수 있는 예외나 오류를 발생시킬 수 있다. 예를 들어, 존재하지 않는 파일을 열려고 하면 `FileNotFoundError` 예외가 발생하고, 접근 권한이 없는 파일에 쓰려고 하면 `PermissionError` 예외가 발생할 수 있다. 또한 이미 닫힌 파일을 읽거나 쓰려고 하면 `ValueError` 예외가 발생하고, 파일의 입력 또는 출력에 문제가 있는 경우 `IOError` 예외가 발생할 수 있다.

이러한 예외와 오류를 처리하기 위해 Python에서 `try`, `except`, `finally`와 `raise` 문을 사용할 수 있다. 이러한 문을 사용하면 파일 작업 중에 발생할 수 있는 예외와 오류를 포착하고 처리할 수 있으며 예외와 오류 발생 여부에 관계없이 일부 코드를 실행할 수 있다. 또한 `raise` 문을 사용하여 자신만의 예외와 오류를 만들고 특정 조건이나 상황에 직면했을 때 이를 발생시킬 수도 있다.

다음은 Python에서 파일 작업의 예외와 오류를 처리하기 위해 이러한 문을 사용하는 방법의 예이다.

```python
# Use the try statement to execute the file operation
try:
    # Use the with statement to open a file in write mode
    with open("example.txt", "w") as file:
        # Write some data to the file
        file.write("This is a test.\n")
# Use the except statement to catch and handle the exceptions and errors
except FileNotFoundError:
    # Print an error message if the file does not exist
    print("The file does not exist.")
except PermissionError:
    # Print an error message if the file is not accessible
    print("You do not have permission to access the file.")
except IOError:
    # Print an error message if there is a problem with the input or output of the file
    print("There is an input or output error.")
# Use the else statement to execute some code if no exception or error occurs
else:
    # Print a success message if the file operation is completed
    print("The file operation is successful.")
# Use the finally statement to execute some code regardless of whether an exception or error occurs or not
finally:
    # Print a message to indicate the end of the file operation
    print("The file operation is done.")
```

보다시피, `try`, `except`, `finally`와 `raise` 문을 사용하면 Python에서 파일 작업의 예외와 오류를 처리할 수 있다. 이러한 문을 사용하면 Python에서 파일 작업을 할 때 프로그램이 충돌하거나 예기치 않게 동작하는 것을 방지할 수 있다.

## <a name="sec_07"></a> Python에서 다양한 파일 형식으로 작업하는 방법
Python은 평문(plain text)과 이진 파일 외에도 CSV, JSON, XML 등 데이터를 저장하고 교환하는 데 일반적으로 사용되는 다양한 파일 형식에서도 작동할 수 있다. 이러한 파일 형식은 고유한 구문과 구조를 가지며, Python에서 다양한 모듈과 라이브러리를 사용하여 구문 분석과 조작이 가능하다.

이 절에서는 Python에서 인기 있는 두 가지 파일 형식인 CSV와 JSON 형식에서 작동하는 방법을 다룰 것이다.

### CSV 파일
CSV는 쉼표로 구분된 값들을 나타낸다. CSV 파일은 스프레드시트나 데이터베이스와 같은 표 형식의 데이터를 저장하는 텍스트 파일이다. CSV 파일의 각 행은 데이터 행을 나타내며, 행의 각 값은 쉼표로 구분된다. 예를 들어, 다음은 책에 대한 몇 가지 정보를 포함하는 샘플 CSV 파일이다.

```
title,author,genre,pages
The Catcher in the Rye,J.D. Salinger,Coming-of-age,234
Pride and Prejudice,Jane Austen,Romance,279
The Hitchhiker's Guide to the Galaxy,Douglas Adams,Science fiction,180
The Da Vinci Code,Dan Brown,Thriller,489
Harry Potter and the Philosopher's Stone,J.K. Rowling,Fantasy,223
```

Python에서 CSV 파일을 작업하려면 `csv` 모듈을 사용하면 된다. `csv` 모듈은 `csv.reader()`, `csv.writer()`, `csv.DictReader()`, `csv.DictWriter()` 등 CSV 파일을 읽고 쓸 수 있는 다양한 함수과 클래스를 제공한다.

다음은 `csv` 모듈을 사용하여 Python에서 CSV 파일을 읽고 쓰는 방법의 예이다.

```python
# Import the csv module
import csv

# Use the with statement to open a CSV file in read mode
with open("books.csv", "r") as file:
    # Create a csv reader object
    reader = csv.reader(file)
    # Skip the header row
    next(reader)
    # Loop through the rows of the CSV file
    for row in reader:
        # Print the title and the author of each book
        print(row[0], "-", row[1])
# Use the with statement to open a CSV file in write mode
with open("books.csv", "w") as file:
    # Create a csv writer object
    writer = csv.writer(file)
    # Write the header row
    writer.writerow(["title", "author", "genre", "pages"])
    # Write some data rows
    writer.writerow(["The Lord of the Rings", "J.R.R. Tolkien", "Fantasy", "1178"])
    writer.writerow(["To Kill a Mockingbird", "Harper Lee", "Southern Gothic", "281"])
    writer.writerow(["Nineteen Eighty-Four", "George Orwell", "Dystopian", "328"])
```

보다시피 `csv` 모듈을 사용하면 쉽고 효율적으로 Python으로 CSV 파일을 읽고 쓸 수 있다. 그러나 `csv` 모듈은 CSV 파일의 인코딩 및 디코딩을 처리하지 않으므로 파일을 열 때 인코딩 및 디코딩 매개 변수를 지정해야 할 수 있다. 또한 `csv` 모듈은 CSV 값의 이스케이프 및 인용을 처리하지 않으므로 리더 및 라이터 객체를 만들 때 `quoting`, `escapechar` 및 `quotechar` 매개 변수를 사용해야 할 수도 있다.

### JSON 파일
JSON은 JavaScript Object Notification의 약자이다. JSON 파일은 구조화되어 있고 사람이 읽을 수 있는 형식으로 데이터를 저장하는 텍스트 파일이다. JSON 파일은 이름과 값의 쌍들의 모음으로 구성되며, 여기서 이름은 문자열이고 값은 문자열, 숫자, 부울, 배열, 객체 또는 널이 될 수 있다. 예를 들어, 다음은 책에 대한 정보를 포함하는 샘플 JSON 파일이다.

```json
[
  {
    "title": "The Catcher in the Rye",
    "author": "J.D. Salinger",
    "genre": "Coming-of-age",
    "pages": 234
  },
  {
    "title": "Pride and Prejudice",
    "author": "Jane Austen",
    "genre": "Romance",
    "pages": 279
  },
  {
    "title": "The Hitchhiker's Guide to the Galaxy",
    "author": "Douglas Adams",
    "genre": "Science fiction",
    "pages": 180
  },
  {
    "title": "The Da Vinci Code",
    "author": "Dan Brown",
    "genre": "Thriller",
    "pages": 489
  },
  {
    "title": "Harry Potter and the Philosopher's Stone",
    "author": "J.K. Rowling",
    "genre": "Fantasy",
    "pages": 223
  }
]
```

Python에서 JSON 파일로 작업하기 위해서는 `json` 모듈을 사용하면 된다. `json` 모듈은 `json.load()`, `json.loads()`, `json.dump()`, `json.dumps()` 등 JSON 데이터를 파싱하고 조작하기 위한 다양한 함수와 메서드를 제공한다.

다음은 Python에서 JSON 파일을 읽고 쓰기 위해 json 모듈을 사용하는 예이다.

```python
 Import the json module
import json

# Use the with statement to open a JSON file in read mode
with open("books.json", "r") as file:
    # Load the JSON data from the file
    data = json.load(file)
    # Loop through the JSON data
    for book in data:
        # Print the title and the author of each book
        print(book["title"], "-", book["author"])
# Use the with statement to open a JSON file in write mode
with open("books.json", "w") as file:
    # Create some JSON data
    data = [
      {
        "title": "The Lord of the Rings",
        "author": "J.R.R. Tolkien",
        "genre": "Fantasy",
        "pages": 1178
      },
      {
        "title": "To Kill a Mockingbird",
        "author": "Harper Lee",
        "genre": "Southern Gothic",
        "pages": 281
      },
      {
        "title": "Nineteen Eighty-Four",
        "author": "George Orwell",
        "genre": "Dystopian",
        "pages": 328
      }
    ]
    # Dump the JSON data to the file
    json.dump(data, file, indent=4)
```

보다시피 json 모듈을 사용하면 쉽고 효율적으로 Python에서 JSON 파일을 읽고 쓸 수 있다. 그러나 `json` 모듈은 JSON 파일의 인코딩과 디코딩을 처리하지 않으므로 파일을 열 때 인코딩과 디코딩 파라미터를 지정해야 할 수도 있다. 또한 `json` 모듈은 JSON 데이터의 포맷과 들여쓰기를 처리하지 않으므로 파일에 데이터를 덤프할 때 `indent`, `sort_keys`와 `separators` 파라메터를 사용해야 할 수도 있다.

이들은 Python에서 서로 다른 파일 형식으로 작업하는 몇 가지 예이다. Python에서 작업할 수 있는 파일 형식에는 XML, HTML, PDF 등 여러 가지가 있다. `xml`, `html`, `pdf` 등과 같이 Python에서 다양한 모듈과 라이브러리를 사용하여 이러한 파일 형식을 구문 분석하고 조작할 수 있다. 이러한 모듈과 라이브러리에 대한 자세한 내용은 [Python documentation](http://localhost:8888/files/development/python_projects/projects/content_bot/%5E5%5E?_xsrf=2%7C5f4d9628%7C92d534f31eaff1c20ab408bb144607f8%7C1704720102) 또는 [Python Package Index](http://localhost:8888/files/development/python_projects/projects/content_bot/%5E6%5E?_xsrf=2%7C5f4d9628%7C92d534f31eaff1c20ab408bb144607f8%7C1704720102)를 참고할 수 있다.

## <a name="summary"></a> 요약
이 포스팅에서는 Python에서 파일을 다루는 방법을 다루었다. 파일을 열고 닫는 방법, 파일을 읽는 방법, 파일을 쓰고 추가하는 방법, 파일 작업에서 예외와 오류를 처리하는 방법, 파일 형식을 달리하여 작업하는 방법 등을 배웠다. Python에서 파일 처리 과정을 단순화하고 개선하기 위해 `with` 문, `csv` 모듈, `json` 모듈을 사용하는 방법도 배웠다.
